
# shortestDistance(1, 2, 3) == = 4.242640687119285
# shortest_distance(134, 191.5, 45.5), 262.47380821712477)
from math import hypot

def shortest_distance_top(*args):
    a, b, c = sorted(args)
    # hypot returns Euclidian norm
    return hypot(a + b, c)

def shortest_distance(a, b, c):
    sides = [a,b,c]
    sides.sort()
    return ((sides[0]+sides[1])**2 + sides[2]**2)**0.5


print(shortest_distance(1,2,3))
