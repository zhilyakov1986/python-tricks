# high_and_low("1 2 3 4 5")  # return "5 1"
# high_and_low("1 2 -3 4 5")  # return "5 -3"
# high_and_low("1 9 3 4 -5")  # return "9 -5"


def high_and_low(str):
    str_list = str.split(' ')
    num_list = [int(i) for i in str_list]
    num_list.sort(reverse=True) 
    return f'{num_list[0]} {num_list[-1]}'

    # return '{} {}'.format(num_list[0], num_list[-1])

print(high_and_low("4 5 29 54 4 0 -214 542 -64 1 -3 6 -6"))

# Test.assert_equals(high_and_low(
#     "4 5 29 54 4 0 -214 542 -64 1 -3 6 -6"), "542 -214")
