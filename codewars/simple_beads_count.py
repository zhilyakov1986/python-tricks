# Two red beads are placed between every two blue beads.
# There are N blue beads. After looking at the arrangement
# below work out the number of red beads.
# B RR B RR B RR B RR B RR B
# Test.assert_equals(count_red_beads(1), 0)
# Test.assert_equals(count_red_beads(3), 4)
# Test.assert_equals(count_red_beads(5), 8)


def count_red_beads(N_blue):
    if N_blue == 0 or N_blue == 1:
        return 0
    else:
        return (N_blue-1)*2

print(count_red_beads(5))
