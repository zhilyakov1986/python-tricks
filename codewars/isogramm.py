def is_isogram(string):
    return len(string) == len(set(string.lower()))


# is_isogram("Dermatoglyphics") == true
# is_isogram("aba") == false
# is_isogram("moOse") =
# = false  # -- ignore letter case
print(is_isogram('aba'))
