# input: arr = [-8, 0, 2, 5]
# output: 2  # since arr[2] == 2

# input: arr = [-1, 0, 3, 6]
# output: -1  # since no index in arr satisfies arr[i] == i.

# def index_equals_value(arr):
#     min_i=[]
#     for i in range(len(arr)):
#         if i == arr[i]:
#             min_i.append(i)
#     if len(min_i):
#         return min(min_i)
#     else:
#         return -1

# import numpy as np
# def index_equals_value(arr):
#     arr = np.array(arr)
#     min_i = -1
#     for i in range(len(arr)):
#         if i == arr[i]:
#             min_i = i
#             break
#         else:
#             continue
#     return min_i


# generator plus list comprehension
def index_equals_value(arr):
    min_i = (i for i in range(len(arr)) if i==arr[i])
    try:
        return next(min_i)
    except StopIteration:
        return -1



print(index_equals_value([-8, 0, 2, 5]))
