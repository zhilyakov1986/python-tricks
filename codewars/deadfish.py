# Write a simple parser that will parse and run Deadfish.

# Deadfish has 4 commands, each 1 character long:

# i increments the value(initially 0)
# d decrements the value
# s squares the value
# o outputs the value into the return array
# parse("iiisdoso") == >  [8, 64]


def parse(data):
    fish = 0
    result = []
    for char in data:
        if char == 'i':
            fish+=1
        if char == 'd':
            fish-=1
        if char == 's':
            fish = fish*fish
        if char == 'o':
            result.append(fish)
    return result

print(parse('iiisdoso'))
