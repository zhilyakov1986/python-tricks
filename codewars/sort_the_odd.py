# You have an array of numbers.
# Your task is to sort ascending odd numbers but even numbers must be on their places.
# Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.
# sort_array([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]
# [1, 3, 2, 8, 5, 11, 4] should equal[1, 3, 2, 8, 5, 4, 11]


def sort_array_old(source_array):
    number_postion = {}
    sorted_array = []
    # store the position of all numbers as a dictionary
    for i in range(len(source_array)):
        number_postion[source_array[i]] = i
    # sort the odd numbers
    sorted_array = [num for num in number_postion.keys() if num % 2]
    sorted_array.sort()
    print(sorted_array)
    # insert even numbers at their previous position
    for key in number_postion.keys():
        if key % 2 == 0:
            sorted_array.insert(number_postion.get(key), key)
    # Return a sorted array.
    return sorted_array


# def sort_array(source_array):
#     L = source_array
#     swap = False
#     while not swap:
#         swap = True
#         for j in range(1, len(L)):
#             if j==0 or j%2:
#                 swap = False
#                 continue
#             elif L[j-1] > L[j]:
#                 swap = False
#                 temp = L[j]
#                 L[j] = L[j-1]
#                 L[j-1] = temp
#     return L

print(sort_array([5, 3, 2, 8, 1, 4]))
