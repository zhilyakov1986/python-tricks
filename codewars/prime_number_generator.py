# sample generator function
# def num_yielder():
#     yield 1
#     yield 2
#     yield 3
#     yield 4

# for i in num_yielder():
#     print(i)  // prints 1,2,3,4

# our_generator = num_yielder()
# calling next(our_generator) each time returns 1 then 2, etc
# print(num_yielder())
# ------------------------------------------------------------------------------

import math
import functools


def is_prime(number):
    if number > 1:
        if number == 2:
            return True
        if number % 2 == 0:
            return False
        # jumps two numbers per iteration only checks odd numbers[]
        for current in range(3, int(math.sqrt(number) + 1), 2):
            if number % current == 0:
                return False
        return True
    return False


def generate_prime():
    num = 1
    while True:
        if is_prime(num):
            yield num
        num += 1


def num_primorial(n):
    list_of_primes = []
    generator = generate_prime()
    while n > 0:
        list_of_primes.append(next(generator))
        n -= 1
    # return list_of_primes
    return functools.reduce(lambda a, b: a*b, list_of_primes)

print(num_primorial(3))
