def KaprekarsConstant(num):
    str_num = str(num)
    kaperakars_num = 6174
    iterations = 0
    while True:
        high_list = sorted(list(str_num), reverse=True)
        low_list = sorted(list(str_num))
        high_num = int(''.join(high_list))
        low_num = int(''.join(low_list))
        new_num = high_num - low_num
        iterations += 1
        if new_num == kaperakars_num:
            break
        else:
            str_num = str(new_num)
            if len(str_num) < 4:
                while len(str_num) < 4:
                    str_num = '0' + str_num
    return iterations

# top solution


def KaprekarsConstantTop(num):
    count = 0
    while num != 6174:
        snum = '{:04d}'.format(num)
        num = int(''.join(sorted(snum, reverse=True))) - \
            int(''.join(sorted(snum)))
        count += 1
    return count


# chess problem: to move pawn diagonaly from bottom left to upper right
def move(a=1, b=1):
    if a == 8 or b == 8:
        return 1
    return move(a+1, b)+move(a, b+1)+move(a+1, b+1)

# or just pass the end coordinate into the function
def path_count(rows, cols):
    if rows == 1 or cols == 1:
        return 1
    return path_count(rows-1, cols) + path_count(rows, cols-1) + path_count(rows-1, cols-1)

# solution if there are no diagonal movements permitted
def factorial(num):
    if num == 1:
        return 1
    return num * factorial(num - 1)
def ChessboardTraveling(str):
    # a is a-x and b is b-y
    a=int(str[6])-int(str[1])
    b=int(str[8])-int(str[3]) 
    return int(factorial(a+b)/factorial(a)/factorial(b))

print(ChessboardTraveling('(2 2)(4 3)'))
