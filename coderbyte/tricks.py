# reverses string
def ReverseStr(str):
    return str[::-1]

# longest word in the string


def LongesWord(sen):
    # splits the string by char into list
    strList = list(sen)
    punctuation = '!@#$%^&*()?.,/[]~:'
    strListModified = []
    strModified = ''

    for i in strList:
        if i not in punctuation:
            strListModified.append(i)
    strModified = ''.join(strListModified)
    wordList = strModified.split()
    longestWord = wordList[0]
    for word in wordList:
        if len(longestWord) < len(word):
            longestWord = word
    return longestWord

# longest word in the string top solution


def LongestWord(sen):
    # takes out punctuation and returns list of words without punctuation
    # isalnum - checks if char is alphanumeric
    words = "".join([",", c][int(c.isalnum())] for c in sen).split(",")
    # get the length of the longest word in the list
    maxLen = max([len(w) for w in words])
    # return the word whose length equal maxLen, return only first result
    return [w for w in words if len(w) == maxLen][0]


def LongestWordTranslate(sen):
    # deletes the chars from the string
    sen = sen.translate(
        None, "'!@#$%^&*()?.,/[]~:'").split()
    big = ''
    for i in sen:
        if len(i) > len(big):
            big = i
    return big

# my solutio


def FirstFactorial(num):
    total = 1
    for i in range(1, num+1):
        total *= i
    return total

# recursive solution


def FirstFactorialRecursive(num):
    if num == 1:
        return 1
    else:
        return num * FirstFactorial(num - 1)

# my solution


def LetterChanges(str):
    alphabet = list('abcdefghijklmnopqrstuvwxyz')
    vowels = list('aeiou')
    letterList = list(str)
    for i in range(len(letterList)):
        if letterList[i] in alphabet:
            # get the index of the next letter in alphabeth
            index = (((alphabet.index(letterList[i]))+1) % len(alphabet))
            letterList[i] = alphabet[index]
            if letterList[i] in vowels:
                letterList[i] = letterList[i].upper()

    return ''.join(letterList)


# using the chr ord
# >> > chr(ord('a')+1)
# 'b'
def LetterChangesOrd(str):
    result = ""
    for char in str:
        if char.isalpha():
            char = chr(ord(char) + 1)
        result = result + char
    return result

# my solution


def SimpleAdding(num):
    total = 0
    for i in range(1, num+1):
        total += i
    return total

# top solution


def SimpleAddingOneLine(num):
    return sum(range(1, num + 1))


def LetterCapitalize(str):
    wordList = str.split()
    result = []
    for i in wordList:
        result.append(i.capitalize())
    return ' '.join(result)

# whithout using capitalize()


def LetterCapitalizeTop(s):
    l = s.split()
    for i in range(len(l)):
        l[i] = l[i][0].upper() + l[i][1:]
    return ' '.join(l)


def SimpleSymbols(str):
    # look for an alphanumeric symbols
    # determine if all symbols surrounded by add signs
    # return string true or false
    result = 'false'
    for i in range(len(str)-1):
        # checks if the char is upper or lower case letter
        if ord(str[i]) >= 97 and ord(str[i]) <= 122 or ord(str[i]) >= 65 and ord(str[i]) <= 90:
            if i != 0 and i != (len(str)-1):
                if str[i-1] == '+' and str[i+1] == '+':
                    result = 'true'
            else:
                result = 'false'
                break
    return result

# top solution


def SimpleSymbols1(str):
    was_plus = 0
    was_letter = 0
    for l in str:
        if l == "+":
            was_plus = 1
            was_letter = 0
        elif l.isalpha():
            if was_plus == 0:
                return "false"
            else:
                was_letter = 1
                was_plus = 0
        else:
            if was_letter == 1:
                return "false"
            else:
                was_plus = 0
                was_letter = 0
    if was_letter == 1:
        return "false"
    return "true"


def SimpleSymbols2(s):
    s = '=' + s + '='
    for i in s:
        if i in 'qwertyuiopasdfghjklzxcvbnm':
            if not s[s.index(i) - 1] == '+' or not s[s.index(i) + 1] == '+':
                return 'false'
    return 'true'


def CheckNums(num1, num2):
    if num2 > num1:
        return 'true'
    elif num1 == num2:
        return '-1'
    elif num1 > num2:
        return 'false'


def TimeConvert(num):
    hours = num//60
    minuts = num % 60
    return "{}:{}".format(hours, minuts)


def AlphabetSoup(str):
    list_str = list(str)
    list_str.sort()
    return ''.join(list_str)

# top
def AlphabetSoupSorted(stri):
  lst = sorted([x for x in stri])
  return ''.join(lst)

